'use strict';

const gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    git = require('gulp-git'),
    paths = ['app.js', 'lib/*'];

module.exports = {
    default: dist,
    dist
};

function dist() {
    return gulp.src(paths)
        .pipe(concat('sp-filters.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({ compress: true }))
        .pipe(rename('sp-filters.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}