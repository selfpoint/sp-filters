(function(angular, app) {
	'use strict';

	app.filter('currency', ['$filter', 'spFilters', function($filter, spFilters) {
		var roundCurrencyFilter = $filter('roundCurrency');

		var filter = function(input, separator, decimalsLength) {
			if (input === undefined || input === null) {
				return '';
			}

			input = Number(input);
			if (isNaN(input)) {
				return '';
			}

			var fixedInput = (roundCurrencyFilter(input) || 0).toFixed(decimalsLength || spFilters.$currencyInnerData.decimalsLength);

			// add space as separator by default for KWD
			if (!separator && spFilters.retailer.currencySymbol === 'KWD') {
				separator = ' ';
			}

			// on euro the symbol should be on the right side and it should be separated by comma instead of dot
			if (spFilters.retailer.currencySymbol === '€') {
				return fixedInput.replace('.', ',') + (separator || '') + spFilters.retailer.currencySymbol;
			}

			return spFilters.retailer.currencySymbol + (separator || '') + fixedInput;
		};

		filter.$stateful = true;

		return filter;
	}]);
})(angular, spFiltersApp);