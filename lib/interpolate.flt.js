(function (angular, app) {
    'use strict';

    app.filter('interpolate', ['$interpolate', function ($interpolate) {
        return function (expression, data, options) {
            options = options || {};
            return $interpolate(expression, !!options.mustHaveExpression, options.trustedContext, !!options.allOrNothing)(data);
        };
    }]);

})(angular, spFiltersApp);