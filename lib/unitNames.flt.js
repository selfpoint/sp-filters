(function (angular, app) {
    'use strict';

    app.filter('unitNames', ['spFilters', function (spFilters) {
        return function (input, defaultNames) {
            if (!input) return '';

            if (input.isWeighable) {
                return defaultNames || spFilters.unitNames.defaultNames;
            }

            if (!input.unitOfMeasure || (!input.unitOfMeasure.names && !input.unitOfMeasure.defaultName)) {
                return '';
            }

            return input.unitOfMeasure.names || input.unitOfMeasure.defaultName;
        };
    }]);


})(angular, spFiltersApp);