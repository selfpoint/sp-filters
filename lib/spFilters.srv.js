(function (angular, app) {
    'use strict';

    app.service('spFilters', function () {
        this.name = {};
        this.unitNames = {};
        this.retailer = {};
        this.missingImage = 'https://d226b0iufwcjmj.cloudfront.net/global/frontend-icons/missing-image.png';
    });
})(angular, spFiltersApp);