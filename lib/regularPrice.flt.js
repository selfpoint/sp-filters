(function (angular, app) {
    'use strict';

    app.filter('regularPrice', ['spFilters', function(spFilters) {
        return function (product, isCase, showPriceWithoutBottleDeposit) {
            if (!product || !product.branch) return 0;
            var regularPrice = 0;
            if (showPriceWithoutBottleDeposit) {
                regularPrice = isCase ? (product.branch.case.price - (product.branch.case.items * product.branch.linkedProductPrice)) : product.branch.regularPrice - product.branch.linkedProductPrice;
            } else {
                regularPrice = isCase ? product.branch.case.price : product.branch.regularPrice;
            }
            if (spFilters.retailer.isRegularPriceWithTax && spFilters.retailer.includeTaxInPrice === false) {
                regularPrice /= 1 + (product.branch.taxAmount || 0);
            } else if (!spFilters.retailer.isRegularPriceWithTax && spFilters.retailer.includeTaxInPrice === true) {
                regularPrice *= 1 + (product.branch.taxAmount || 0);
            }
            return regularPrice;
        };
    }]);
})(angular, spFiltersApp);
