(function (angular, app) {
    'use strict';

    app.filter('title', ['spFilters', '$filter', 'Config', function (spFilters, $filter, Config) {
        function filter (input) {
            if (!input) {
                return '';
            }

            if (typeof input !== 'string') {
                return input;
            }

            var title;
            if (Config.retailer.metaTitle) {
                title = Config.retailer.metaTitle;
            }
            else if (Config.retailer.shopTitle) {
                title = $filter('name')(Config.retailer.shopTitle);
            }
            else {
                title = Config.retailer.title
            }

            if (input.indexOf('|') !== -1) {
                return input.replace(/\|.*/, '| ' + title);
            }
            else {
                return title;
            }
        }

        filter.$stateful = false;
        return filter;
    }]);

})(angular, spFiltersApp);