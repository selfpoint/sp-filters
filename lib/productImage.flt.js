(function (angular, app) {
    'use strict';

    /**
     * @typedef {Object} SPProductImage
     *
     * @property {string} url
     * @property {boolean} isSilent
     */

    app.filter('productImage', ['$filter', 'spFilters', function ($filter, spFilters) {
        var _imageFilter = $filter('image');

        /**
         * Product image filter
         * @public
         *
         * @param {Object} product
         * @param {SPProductImage} product.image
         * @param {Object} options
         * @param {boolean} [showSilent=false]
         *
         * @returns {String}
         */
        return function productImageFilter(product, options, showSilent) {
            return _imageFilter(_isShownImage(product && product.image, showSilent) && product.image.url, options);
        };
    }]);

    app.filter('productImages', ['$filter', 'spFilters', function ($filter, spFilters) {
        /**
         * Product image filter
         * @public
         *
         * @param {Object} product
         * @param {SPProductImage} product.image
         * @param {Array<SPProductImage>} product.images
         * @param {boolean} [showSilent=false]
         *
         * @returns {String}
         */
        return function productImageFilter(product, showSilent) {
            var images = [];

            if (!product) {
                return images;
            }

            var runImages = [product.image];
            Array.prototype.push.apply(runImages, product.images || []);

            angular.forEach(runImages, function(image) {
                if (_isShownImage(image, showSilent)) {
                    images.push(image);
                }
            });

            return images;
        };
    }]);

    /**
     * Returns whether the given image should be shown
     * @private
     *
     * @param {SPProductImage} image
     * @param {boolean} showSilent
     *
     * @return {boolean}
     */
    function _isShownImage(image, showSilent) {
        return !!image && !!image.url && (!!showSilent || !image.isSilent);
    }
})(angular, spFiltersApp);
