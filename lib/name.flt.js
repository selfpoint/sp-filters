(function (angular, app) {
    'use strict';

    var ENGLISH = {
        id: 2,
        culture: 'en'
    };

    app.filter('name', ['spFilters', function (spFilters) {
        function filter (input, innerProperty) {
            if (!input) {
                return '';
            }

            if (typeof input === 'string') {
                return input;
            }

            // first prioritize the current chosen language
            var languages = [spFilters.name.language.id, spFilters.name.language.culture];

            // then prioritize the retailer's default language
            if (spFilters.name.retailerLanguage) {
                // if default language found - push the language id into languages list
                if (spFilters.name.retailerLanguage.id) {
                    languages.push(spFilters.name.retailerLanguage.id)
                }
                // if default language found - push the language culture into languages list
                if (spFilters.name.retailerLanguage.culture) {
                    languages.push(spFilters.name.retailerLanguage.culture)
                }
            }

            // then prioritize english over other languages existing on the input
            languages.push(ENGLISH.id, ENGLISH.culture);

            // and then all other languages on the input
            Array.prototype.push.apply(languages, Object.keys(input));

            var i,
                result;
            for (i = 0; i < languages.length; i++) {
                result = input[languages[i]];
                if (result && innerProperty) {
                    result = result[innerProperty];
                }
                if (result) {
                    return angular.isArray(result) ? result[0] : result;
                }
            }
        }
        filter.$stateful = true;
        return filter;
    }]);

})(angular, spFiltersApp);
