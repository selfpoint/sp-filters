(function(angular, app) {
	'use strict';

	var PRICE_ROUND_MODES = {
		ROUND: 1,
		CEILING: 2,
		FLOOR: 3
	};

	// enough precision to not hurt the actual number yet low enough to remove the js bug precision
	var _jsBugMultiplier = 10000000000;

	var _lastRoundModeId,
		_roundModeOperation;

	app.filter('roundCurrency', ['spFilters', function(spFilters) {
		function filter(input) {
			if (input === undefined || input === null) {
				return input;
			}

			var numInput = Number(input);
			if (isNaN(numInput)) {
				return input;
			}

			// set the round operation locally by the price round mode
			_setPriceRoundModeOperation();

			// parse and set data about the currency minor unit for later use and for the currency filter to use
			_setMinorUnitData();

			// first, multiply num by the _jsBugMultiplier and round it
			// all next actions should be made on a rounded integer
			// to avoid mistakes with numbers affected by js float bug (1.16 / 0.005 = 231.99999999999997 for example)
			numInput = Math.round(numInput * _jsBugMultiplier);

			// then round the amount to the minor unit
			numInput = _roundModeOperation(numInput / spFilters.$currencyInnerData.multipliedMinorUnit) *
				spFilters.$currencyInnerData.multipliedMinorUnit;

			// then divide back to the real number
			return numInput / _jsBugMultiplier;
		}

		function _setPriceRoundModeOperation() {
			if (_lastRoundModeId === spFilters.retailer.priceRoundModeId && _roundModeOperation) {
				return;
			}

			_lastRoundModeId = spFilters.retailer.priceRoundModeId;
			switch(_lastRoundModeId) {
				case PRICE_ROUND_MODES.CEILING:
					_roundModeOperation = Math.ceil;
					break;
				case PRICE_ROUND_MODES.FLOOR:
					_roundModeOperation = Math.floor;
					break;
				default:
					_roundModeOperation = Math.round;
			}
		}

		function _setMinorUnitData() {
			var minorUnit = spFilters.retailer.currencyMinorUnit || 0.01;

			if (spFilters.$currencyInnerData && minorUnit === spFilters.$currencyInnerData.minorUnit &&
				spFilters.$currencyInnerData.decimalsLength) {
				return;
			}

			spFilters.$currencyInnerData = {
				minorUnit: minorUnit,
				multipliedMinorUnit: Math.round(minorUnit * _jsBugMultiplier),
				decimalsLength: minorUnit.toString().split('.')[1].length
			};
		}

		filter.$stateful = true;
		return filter;
	}]);
})(angular, spFiltersApp);