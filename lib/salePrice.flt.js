(function (angular, app) {
    'use strict';

    app.filter('salePrice', ['spFilters', function(spFilters) {
        return function (product) {
            var salePrice = product.branch && product.branch.salePrice;
            if (spFilters.retailer.isRegularPriceWithTax && spFilters.retailer.includeTaxInPrice === false) {
                salePrice /= 1 + (product.branch && product.branch.taxAmount || 0);
            } else if (!spFilters.retailer.isRegularPriceWithTax && spFilters.retailer.includeTaxInPrice === true) {
                salePrice *= 1 + (product.branch && product.branch.taxAmount || 0);
            }
            return salePrice;
        };
    }]);
})(angular, spFiltersApp);