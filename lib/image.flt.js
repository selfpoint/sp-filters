(function (angular, app) {
    'use strict';

    app.filter('image', ['$filter', 'spFilters', function ($filter, spFilters) {
        var _interpolateFilter = $filter('interpolate');

        /**
         * Image filter
         * @public
         *
         * @param {String} url
         * @param {Object|String} [option={size: 'medium'}]
         * @param {String} options.size
         * @param {String} [options.extension]
         *
         * @returns {String}
         */
        return function (url, options) {
            if (options === undefined) {
                options = {size: 'medium'};
            } else if (typeof options == 'string') {
                options = {size: options};
            }

            url = _interpolateFilter(url || spFilters.missingImage, options);

            if (url.indexOf('http') == 0 || url.indexOf('mobilezuz') != -1 || url.indexOf('//') == 0) {
                return url;
            } else if (url.indexOf('/') == 0) {
                return 'https://ecom.blob.core.windows.net' + url;
            } else {
                return 'https://ecom.blob.core.windows.net/' + url;
            }
        };
    }]);
})(angular, spFiltersApp);
