(function (angular, app) {
    'use strict';

    app.filter('productWeight', ['$filter', function ($filter) {
        var _unitNamesFilter = $filter('unitNames'),
            _nameFilter = $filter('name'),
            _spUnitsFilter;

        try {
            _spUnitsFilter = $filter('spUnits');
        } catch (e) { }

        return function (input, isCase, separator, quantity, soldBy) {
            if (!angular.isObject(input)) return input;
            if (quantity === 0) return quantity;

            var unitOfMeasure = _nameFilter(_unitNamesFilter(input));
            var weight;
            if (soldBy === 'Weight') {
                weight = (quantity || 1) * ((quantity && 1) || input.weight || 0);
            } else {
                weight = (quantity || 1) * (input.weight || (quantity && 1) || 0);
            }
            if (isCase && input.branch && input.branch.case && input.branch.case.items && input.isWeighable) {
                weight = input.branch.case.items;
            } else if (isCase && input.branch && input.branch.case && input.branch.case.items) {
                weight *= input.branch.case.items;
            }

            var prefix = (input.numberOfItems && input.numberOfItems > 1 ? input.numberOfItems + '×' : '');

            if (_spUnitsFilter) {
                var options = {
                    separator: separator,
                    convertInGroup: input.isWeighable && ((soldBy === 'Weight') || (!input.weight))
                };
                return prefix + _spUnitsFilter(weight, unitOfMeasure, options);
            }

            return angular.isDefined(weight) ? prefix + weight + (separator || ' ') + unitOfMeasure : '';
        };
    }]);


})(angular, spFiltersApp);