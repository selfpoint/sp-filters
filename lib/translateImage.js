(function (angular, app) {
    'use strict';

    app.filter('translateImage', [
        'Config',
        function (config) {
            function filter(input, languages) {
                if (languages && angular.isString(input)) {
                    if (config.language.culture && languages[config.language.culture]) {
                        return input.replace(/%translate%/g, languages[config.language.culture]);
                    }
                }

                return input.replace(/%translate%/g, '');
            }

            filter.$stateful = true;
            return filter;
        }]);
})(angular, spFiltersApp);